﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ParsPack.Data;
using RestSharp;
using RestSharp.Authenticators;

namespace ParsPack
{
    public partial class frmMain : Form
    {
        static HttpClient client = new HttpClient();
        List<Data.Data> _sampleDataList = new List<Data.Data>();
        List<Data.Data> _resultrecords;
        int currentPage = 0;
        int currentSize = 12;
        public frmMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
         
        private static async Task NewMethod()
        {
            await using (var db = new AuthDbContext())
            {
                await db.Database.EnsureCreatedAsync();
                var beer1 = new Data.Data()
                {
                    name = "user_zar",
                    date = "2015-03-28",
                    comment = "5.7.6"
                };
                db.Datas.Add(beer1);
                await db.SaveChangesAsync();
            }
        }

        private async void btnGetData_Click(object sender, EventArgs e)
        {
            var data = GetDataAsync(Program.path);
            await ClearTable();
            await FillDatabase(await data);
            MessageBox.Show("Filled Local Databse");
            btnFillDataFromSQLite.Enabled = true;
        }
        static async Task<List<Data.Data>> GetDataAsync(string path)
        {
            RestClient client = new RestClient(path);
            RestRequest request = new RestRequest("", Method.GET, DataFormat.Json);
            IRestResponse response = client.Execute(request);
            response.ContentType = "application/json";
            var model = JsonConvert.DeserializeObject<List<Data.Data>>(response.Content);
            
            return model;
        }

        private static async Task FillDatabase(List<Data.Data> model)
        {
            await using (var db = new AuthDbContext())
            {
                await db.Database.EnsureCreatedAsync();

                db.Datas.AddRange(model);
                await db.SaveChangesAsync();
               
            }
        }
        private  async Task ReadDatabase()
        {
            await using (var db = new AuthDbContext())
            {
                await db.Database.EnsureCreatedAsync();
                dgv.DataSource=_sampleDataList=db.Datas.ToList();
                
            }
            
        }

        private static async Task ClearTable()
        {
            await using (var db = new AuthDbContext())
            {
                await db.Database.EnsureCreatedAsync();

                db.Datas.RemoveRange(db.Datas);
                await db.SaveChangesAsync();
            }
        }
        private int TotalPages()
        {
         return (_resultrecords.Count() / currentSize)+1;
        }
        private async void btnClearTable_Click(object sender, EventArgs e)
        {
            await ClearTable();
            MessageBox.Show("Clearing the Local Table");
        }

        private async void btnFillGridVeiw_Click(object sender, EventArgs e)
        {
            var data = GetDataAsync(Program.path);
            _resultrecords=_sampleDataList = data.Result;
            DataGridViewCell cell = new DataGridViewTextBoxCell();

            var bindingList = new BindingList<Data.Data>(data.Result);
            var source = new BindingSource(bindingList, null);

            dgv.DataSource = data.Result;
            dgv.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            SetPagingOption(1, TotalPages());
            MessageBox.Show("Fill a GridView directly from the API");
        }

        private void btnNewRecord_Click(object sender, EventArgs e)
        {
            new frmAddRecord().ShowDialog();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
             using (var db = new AuthDbContext())
            {
                try
                {
                    dgv.DataSource = null; dgv.Rows.Clear(); dgv.Refresh();
                    if (!string.IsNullOrEmpty( txtId.Text))
                        _resultrecords = Filter.PerformFilter(ref _sampleDataList, (a) => a.id == int.Parse(txtId.Text) && a.name.StartsWith(txtName.Text) && a.date.StartsWith(txtDate.Text) && a.comment.StartsWith(txtComment.Text));
                
                    else
                        _resultrecords = Filter.PerformFilter(ref _sampleDataList, (a) =>   a.name.StartsWith(txtName.Text) && a.date.StartsWith(txtDate.Text) && a.comment.StartsWith(txtComment.Text));
                    SetPagingOption(0, TotalPages());
                   
                    //var query = from VAR in db.Datas where VAR.name.Contains(txtName.Text)


                }
                catch (Exception ex)
                {

                  
                }
            }
        }

        private void btnLastPage_Click(object sender, EventArgs e)
        {
            currentPage = (_resultrecords.Count() / currentSize) - 1;

            SetPagingOption(currentPage, TotalPages());
        }
        private void Paging(int pagenum, int pagesize)
        {

            if (currentPage < 0) { currentPage = 0; return; }

            var records = from p in _resultrecords.Skip(pagenum * pagesize).Take(pagesize)
                           select new { p.id,p.name,p.date,p.comment };

            dgv.DataSource = records.ToList();
            lblCurentPage.Text = (currentPage+1).ToString();
        }

        private void btnNextPage_Click(object sender, EventArgs e)
        {
            currentPage = ((currentPage + 1) * currentSize) < _resultrecords.Count ?
               (currentPage + 1) : currentPage;
            SetPagingOption(currentPage, TotalPages());

        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
           
            SetPagingOption((currentPage - 1) > 0 ? (currentPage - 1) : 0, TotalPages());
        }

        private void btnFirstPage_Click(object sender, EventArgs e)
        {
            SetPagingOption(0, TotalPages());
        }

        private void btnGoToPage_Click(object sender, EventArgs e)
        {
            var curentpg = numericUpDown1.Value.ToString();
            int t = int.Parse(curentpg);
            if (0 < t && t < TotalPages())
                SetPagingOption(t-1, TotalPages());
           
        }

        private void btnClearfilter_Click(object sender, EventArgs e)
        {
            ClearFilters();
            ClearGridData();
            SetPagingOption(0, TotalPages());
            
        }
        private void ClearFilters()
        {
            try
            {
                txtComment.Text = string.Empty;
                txtName.Text = string.Empty;
                txtId.Text = string.Empty;
                txtDate.Text = string.Empty;
            }
            catch (Exception ex)
            {

            }
        } 
        private void ClearGridData()
        {
            try
            {
                dgv.DataSource = null;
                dgv.Rows.Clear();
                dgv.Refresh();
                dgv.DataSource = _resultrecords=_sampleDataList;
            }
            catch (Exception ex)
            {

            }
        }   
        private void SetPagingOption(int currentpage,int totalpage)
        {
            try
            {
                lblTotalPage1.Text = TotalPages().ToString();
                Paging(currentpage, currentSize);
            }
            catch (Exception ex)
            {

                
            }
        }

        private async void btnFillDataFromSQLite_Click(object sender, EventArgs e)
        {
            try
            {
                await ReadDatabase();
                MessageBox.Show("Read from Databse");
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
