﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using ParsPack.Data;
using RestSharp;

namespace ParsPack
{
    public partial class frmAddRecord : Form
    {
        public frmAddRecord()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtName.Text))
                if(string.IsNullOrEmpty(txtDate.Text))
                    if (string.IsNullOrEmpty(txtDate.Text))
                    {
                        MessageBox.Show("Not all fields are complete correctly", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
            var isSuccessful= AddDataAsync(Program.path, txtName.Text, txtDate.Text, txtComment.Text);
            
            if (isSuccessful.Result)
            {
                MessageBox.Show("Add new Record", "Successful",MessageBoxButtons.OK,MessageBoxIcon.Information);
                txtName.Text = txtDate.Text = txtComment.Text = string.Empty;
            }
            else
            {
                MessageBox.Show("The operation was not successful!","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }

            
        }
        static async Task<bool> AddDataAsync(string path,string name,string date,string comment)
        {

            RestClient client = new RestClient(path);
            RestRequest request = new RestRequest("", Method.POST);
            request.AddQueryParameter("name", name);
            request.AddQueryParameter("date", date);
            request.AddQueryParameter("comment", comment);
            IRestResponse response = client.Execute(request);
            return response.IsSuccessful;
        }
        private static async Task FillDatabase(Data.Data model)
        {
            await using (var db = new AuthDbContext())
            {
                await db.Database.EnsureCreatedAsync();

                db.Datas.Add(model);
                await db.SaveChangesAsync();
            }
        }
    }
}
