﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ParsPack.Data
{
    public class AuthDbContext:DbContext
    {
        public DbSet<Data> Datas { get; set; }

        public AuthDbContext(DbContextOptions<AuthDbContext> options) : base(options)
        {

        }

        public AuthDbContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(connectionString: "Filename=" + Program.database,
                sqliteOptionsAction: op => { op.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName); });
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Data>().ToTable("Datas");
            modelBuilder.Entity<Data>(entity =>
            {
                entity.Property(e => e.id);
                entity.Property(e => e.name).HasMaxLength(250);
                entity.Property(e => e.date).HasMaxLength(250);
                entity.Property(e => e.comment).HasMaxLength(250);
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}
