﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParsPack.Data
{
    public class Data
    {
        [DisplayName("Id")]
        public int id { get; set; }
        [DisplayName("Name")]
        public string name { get; set; }
        [DisplayName("Date")]
        public string date { get; set; }
        [DisplayName("Comment")]
        public string comment { get; set; }
    }
}
