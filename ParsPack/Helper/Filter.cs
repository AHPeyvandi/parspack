﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ParsPack
{
    public static class Filter
    {
        public static List<Data.Data> PerformFilter(ref List<Data.Data> sourceItems, Func<Data.Data, bool> validateFunc)
        {
            return sourceItems.Where(a => validateFunc.Invoke(a)).ToList();


        }

    }
}
